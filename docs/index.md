# Présentation
---------------

**Ce site présente les principales fonctionnalités de pgAdmin4 Server**

pgAdmin4 est le principal outil de gestion Open Source pour PostgreSQL. Il fournit une interface graphique pour simplifier la création, la maintenance et l'utilisation des objets de votre base de données.

Les principales fonctionnalités de pgAdmin4 Server vous sont présentées:

* pour une utilisation dans le cadre de l'offre Eole PostgreSQL
* pour une utilisation dans le cadre de la formation à distance "Administrer ses données avec PostgreSQL
