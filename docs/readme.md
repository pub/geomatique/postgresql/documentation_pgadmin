# pgAdmin Server : gestion des objets pour PostgreSQL

Bienvenue dans la documentation pour pgAdmin Serveur. Cet outil, proposé dans l'offre Eole en mode web, est l'un des principaux outils Open Source pour PostgreSQL. 
Au travers de ces interfaces graphiques, il vous simplifie la création, la maintenance et l'utilisation des objets de votre base de données.
 
 En mode serveur, pgAdmin est une application web qui vous permet d'accéder à votre serveur Eole PostgreSQL y compris via le VPN et vous dispense de multiples installations monopostes pour les différents utilisateurs de PostgreSQL en agissant, par utilisateur, comme un trousseau de connexions vers vos bases de données.
 
 ## Environnement

Lors de l'installation du serveur Eole variante géomatique (offre PostgreSQL), l'application pgAdmin Serveur est installée et accessible dans votre navigateur à l'adresse **http://adresse_ip_de_votre_serveur/pgadmin4**

La page d'acceuil vous demande un compte de connexion et un mot de passe. Ce premier compte de connexion doit vous permettre de vous déclarer dans la base des utilisateurs de pgAdmin. 
**Après avoir créé votre compte et testé votre connexion, il est fortement conseillé de supprimer le premier compte de connexion provisoire**

## Documentation
Documentation utilisateur pour Eole :  

Documentation utilisateur pgAdmin en anglais : https://www.pgadmin.org/docs/pgadmin4/7.8/index.html

## Crédits
© The pgAdmin Development Team, 2013 - 2021.
© République Française, 2022-2023.


### Éditeur de cette documentation
Direction du numérique du Ministère de la Transition écologique et de la Cohésion des territoires, du Ministère de la Transition énergétique et du Secrétariat d'État chargé de la Mer.


### Licence PostgreSQL
Conditions d'utilisation de la licence PostgreSQL :  https://www.pgadmin.org/licence/

IN NO EVENT SHALL THE PGADMIN DEVELOPMENT TEAM BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE PGADMIN DEVELOPMENT TEAM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE PGADMIN DEVELOPMENT TEAM SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE PGADMIN DEVELOPMENT TEAM HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.