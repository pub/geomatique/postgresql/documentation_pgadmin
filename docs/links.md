# Les sites utiles
---------------

## Références

* Documentation sur les connexions à PostgreSQL: [connexions à PostgreSQL avec pgAdmin, QGIS, Libre Office](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/postgresql/documentation_connexion_postgresql/)
  
* Site communautaire pgAdmin : [site pgadmin](https://www.pgadmin.org/)

* Documentation pgAdmin : [site documentaire](http://10.167.71.3/pgadmin4/help/help/index.html?ver=50300#)

